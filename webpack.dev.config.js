var webpack = require('webpack');
var path = require('path');
var DashboardPlugin = require('webpack-dashboard/plugin');

let HtmlWebpackPlugin = require('html-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
	entry: APP_DIR + '/index.jsx',

	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},

	devtool: 'cheap-module-source-map',

	module : {
		rules : [{
			test : /\.(js|jsx)?/,
			exclude: /(tests|dist)/,
			include : [
				APP_DIR
			],
			use: [
				`babel-loader`
			]
		}, {
			test: /\.html$/,
			loader: 'html-loader',
			options: {
				minimize: true
			}
		}]
	},

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			hash: true,
			filename: 'index.html',
			template: APP_DIR + '/index.html'
		}),
		new DashboardPlugin()
	],

	devServer: {
		port: 3000,
		inline: true,
		hot: true,
		compress: true,
		noInfo: true,
		contentBase: BUILD_DIR
	}
};

module.exports = config;