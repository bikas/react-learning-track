import App from "./app.jsx";

describe('App', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallow(
			<App />
		);
	});

	it('should render', () => {
		expect(wrapper).toBeTruthy();
	});

	it('should have h1', () => {
		expect(wrapper.find('h1').length).toBe(1);
	});
});