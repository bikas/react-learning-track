let webpack = require('webpack');
let path = require('path');

let HtmlWebpackPlugin = require('html-webpack-plugin');

const BUILD_DIR = path.resolve(__dirname, 'dist');
const APP_DIR = path.resolve(__dirname, 'src');

const BABEL_QUERY = {
	presets: [
		'latest',
		'react'
	],
	plugins: [
		'transform-react-constant-elements',
		'transform-react-inline-elements'
	]
};

module.exports = {
	entry: APP_DIR + '/index.jsx',

	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},

	devtool: false,

	module : {
		rules : [{
			test : /\.(js|jsx)?/,
			exclude: [
				/(tests_preset|dist)/,
				path.resolve(__dirname, 'src/**/*.spec.js'),
				path.resolve(__dirname, 'src/**/*.spec.jsx')
			],
			include : [
				APP_DIR
			],
			use: [
				`babel-loader?${JSON.stringify(BABEL_QUERY)}`
			]
		}, {
			test: /\.html$/,
			loader: 'html-loader',
			options: {
				minimize: true
			}
		}]
	},

	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production')
			}
		}),
		new HtmlWebpackPlugin({
			hash: true,
			filename: 'index.html',
			template: APP_DIR + '/index.html'
		})
	]
}